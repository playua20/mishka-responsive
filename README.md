# «Mishka»

  *  [https://mishka-three.vercel.app/](https://mishka-three.vercel.app/) - Главная
  *  [https://mishka-three.vercel.app/](https://mishka-three.vercel.app/works.html) - Каталог товаров
  *  [https://mishka-three.vercel.app/](https://mishka-three.vercel.app/order.html) - Страница заказа

 >  Устаревший free домен (2017): 
     
 > * [http://mishka.kl.com.ua/](http://mishka.kl.com.ua/) - Главная
 
 > * [http://mishka.kl.com.ua/](http://mishka.kl.com.ua/works.html) - Каталог товаров
     
 > * [http://mishka.kl.com.ua/](http://mishka.kl.com.ua/order.php) - Страница заказа

## Особенности:
 * Responsive
 * Mobile first
 * BEM
 * PHP form (больше не работает - http://mishka.kl.com.ua/order.php)
 
## Технологии:
 * jQuery
 * Bootstrap-grids
 * Gulp
 * Scss

 > *Дизайн шаблона принадлежит ***HtmlAcademy****

@ 2018-2023
